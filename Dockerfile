FROM cern/alma9-base

# add epel repos
RUN yum install -y epel-release

RUN yum install -y cmake make gcc-c++ git boost-devel \
        cppcheck cppunit-devel lcov \
        pugixml-devel sqlite-devel \
        openssl-devel \
        clang clang-tools-extra && \
    yum clean all

# Enable kerberos authentication (https://gitlab.docs.cern.ch/docs/User%20account%20options/authentication#kerberos-authentication)
RUN git config --global http.emptyAuth true


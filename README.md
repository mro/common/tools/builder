# MRO build env

Docker images for MRO projects

## Install

To use the docker build image:
```bash
docker pull gitlab-registry.cern.ch/mro/common/tools/builder

# Will bind current directory to /root in docker container
docker run --rm -it -v $PWD:/root -w /root gitlab-registry.cern.ch/mro/common/tools/builder bash
```

## docker-builder Script

A convenience script is provided to start the proper Docker environment for a given project, the script and installation procedure can be found here: [x-builder](https://gitlab.cern.ch/mro/common/tools/x-builder)

This script will look for a .docker-builder file in current directory and spawn a docker container mounting proper directories:
```bash
echo gitlab-registry.cern.ch/mro/common/tools/builder > .docker-builder
```

To use this script:
```bash
# From a project directory
x-builder bash
```
